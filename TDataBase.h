#ifndef DATABASE_H
#define	DATABASE_H

#include <string>
#include <iostream>
#include <fstream>

#include "TStorage.h"

#include <QTableWidget>

using namespace std;

class TDataBase {
public:
    // constructors
    TDataBase() : m_has_modified(true) 
    {};
    
    TDataBase(const TDataBase& orig) : m_storage(orig.m_storage),
        m_has_modified(orig.m_has_modified)
    {};
    
    // database item
    struct SSoftwarePacket {
        string name;
        string developer;
        string vendor;
        string version;
        string packet_size;
        string date;
        
        SSoftwarePacket() {};
        SSoftwarePacket(string n, string d, string vn, string vr, string ps, string dt) : 
                name(n), developer(d), vendor(vn), version(vr), packet_size(ps),
                date(dt)
        {}
        SSoftwarePacket(SSoftwarePacket &orig) : name(orig.name),
                developer(orig.developer), vendor(orig.vendor), 
                version(orig.version), packet_size(orig.packet_size),
                date(orig.date)
        {}
        
        bool operator == (SSoftwarePacket &right) {
            return 
                    name == right.name &&
                    developer == right.developer &&
                    vendor == right.vendor &&
                    version == right.version &&
                    packet_size == right.packet_size &&
                    date == right.date;
        }
        
        SSoftwarePacket& operator=(const SSoftwarePacket& orig) {
            if (this == &orig) {
                return *this;
            }
            
            name = orig.name;
            developer = orig.developer;
            vendor = orig.vendor;
            version = orig.version;
            packet_size = orig.packet_size;
            date = orig.date;
            
            return *this;
        }
        
        friend ostream& operator << (ostream &stream, SSoftwarePacket &object) {
            stream << object.name << " "
                    << object.developer << " " 
                    << object.vendor << " " 
                    << object.version << " "
                    << object.packet_size << " "
                    << object.date;
            return stream;
        }
        
        friend ifstream& operator >> (ifstream &stream, SSoftwarePacket &object) {
            stream >> object.name >> object.developer >> object.vendor 
                    >> object.version >> object.packet_size >> object.date;
            return stream;
        }
    };
    
    // inline getters
    bool is_modified() const { return m_has_modified; }
    
    // methods
    void add(SSoftwarePacket &item);
    void remove(int index);
    void edit(int index, SSoftwarePacket &new_item);

    int find(int start_index, string pattern) const;
    
    void merge(string file_name);
    
    void save(string file_name);
    void load(string file_name);

    SSoftwarePacket &get_item(int index) const;

    // print
    void output_db(QTableWidget &out) const;
    
    // destructor
    virtual ~TDataBase();
private:
    TStorage <SSoftwarePacket> m_storage;
    bool m_has_modified;
};

#endif	/* DATABASE_H */

