#ifndef SWMAINWINDOW_H
#define SWMAINWINDOW_H

#include <QMainWindow>
#include "ui_mainwindow.h"

#include "TDataBase.h"

namespace Ui {
    class SWMainWindow;
}

class SWMainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit SWMainWindow(QWidget *parent = 0);
    ~SWMainWindow();

signals:
    
public slots:

private slots:
    void on_actionCredits_triggered();
    void on_actionExit_triggered();
    void on_actionNew_triggered();
    void on_actionAdd_triggered();
    void on_actionRemove_triggered();
    void on_actionEdit_triggered();
    void on_actionSave_triggered();
    void on_actionClose_triggered();
    void on_actionOpen_database_triggered();
    void on_actionMerge_triggered();
    void on_actionFind_triggered();

private:
    Ui::MainWindow *ui;
    QStatusBar *m_status_bar;

    TDataBase *m_data_base;

    void ask_for_save();
};

#endif // SWMAINWINDOW_H
