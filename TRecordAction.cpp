#include "TRecordAction.h"

TRecordAction::TRecordAction(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::rec_action_form)
{
    ui->setupUi(this);
}

TDataBase::SSoftwarePacket* TRecordAction::get_form_data() const {
    TDataBase::SSoftwarePacket *sp = new TDataBase::SSoftwarePacket();

    sp->name = ui->name_lineEdit->text().toStdString();
    sp->developer = ui->devel_lineEdit->text().toStdString();
    sp->version = ui->version_lineEdit->text().toStdString();
    sp->vendor = ui->vendor_lineEdit->text().toStdString();
    sp->packet_size = ui->psize_lineEdit->text().toStdString();
    sp->date = ui->date_lineEdit->text().toStdString();

    if (sp->name == "" || sp->developer == "" || sp->version == "" ||
            sp->vendor == "" || sp->packet_size == "" || sp->date == "")
        return nullptr;

    return sp;
}

void TRecordAction::load_form_data(int id, TDataBase::SSoftwarePacket sp) {
    ui->id_label->setText(QString::number(id));

    ui->name_lineEdit->setText(QString::fromStdString(sp.name));
    ui->devel_lineEdit->setText(QString::fromStdString(sp.developer));
    ui->version_lineEdit->setText(QString::fromStdString(sp.version));
    ui->vendor_lineEdit->setText(QString::fromStdString(sp.vendor));
    ui->psize_lineEdit->setText(QString::fromStdString(sp.packet_size));
    ui->date_lineEdit->setText(QString::fromStdString(sp.date));
}
