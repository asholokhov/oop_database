HEADERS += \
    TStorage.h \
    TDataBase.h \
    TRecordAction.h \
    SWMainWindow.h

SOURCES += \
    TDataBase.cpp \
    main.cpp \
    TRecordAction.cpp \
    SWMainWindow.cpp

FORMS += \
    record_action.ui \
    mainwindow.ui

QMAKE_CXXFLAGS += -std=c++0x
