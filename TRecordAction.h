#ifndef TRECORDACTION_H
#define TRECORDACTION_H

#include "ui_record_action.h"
#include "TDataBase.h"

namespace Ui {
    class TRecordAction;
}

class TRecordAction : public QDialog
{
    Q_OBJECT
public:
    explicit TRecordAction(QWidget* parent = 0);
    TDataBase::SSoftwarePacket* get_form_data() const;
    void load_form_data(int id, TDataBase::SSoftwarePacket sp);

private:
    Ui::rec_action_form *ui;
};

#endif // TRECORDACTION_H
