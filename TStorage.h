#ifndef TSTORAGE_H
#define	TSTORAGE_H

#include <iostream>
using namespace std;

template <typename TValue, int t_capacity = 0>
class TStorage {
public:
    // constructors
    TStorage() : m_container(nullptr), m_capacity(t_capacity), m_size(0)
    {};
    
    TStorage(const TStorage& orig);
    
    // methods
    void push_back(TValue &value);
    void remove(int index);
    void replace(int index, TValue &item);
    
    // output to std
    void print_all() const;
    
    // iterator
    struct iterator {
        iterator (TValue* ptr = nullptr) :
            m_ptr(ptr)
        {}

        TValue& operator*()  { return *m_ptr; }
        TValue* operator->() { return m_ptr;  }
        TValue* operator++() { return ++m_ptr;}
        TValue* operator--() { return --m_ptr;}

        bool operator==(const iterator& other) const { return m_ptr == other.m_ptr; }
        bool operator!=(const iterator& other) const { return !(*this == other); }
        
    private:
        TValue *m_ptr;
    };
    
    // inline getters
    int  size()    const { return m_size;      }
    bool empty()   const { return m_size == 0; }
    
    TValue* begin() const { return m_container; }
    TValue* end()   const { return m_container + m_size; }
    
    // operators
    TValue& operator[](int index) const {
        return m_container[index];
    }
    
    // exceptions
    class EIndexOutOfBounds {};
    
    // destructor
    virtual ~TStorage();
private:
    TValue *m_container;
    
    int m_capacity;
    int m_size;
    
    void expand();
};

template <typename TValue, int t_capacity>
TStorage<TValue, t_capacity>::TStorage(const TStorage& orig) :
        m_capacity(orig.m_capacity), m_size(orig.m_size) {
    m_container = new TValue[m_capacity];
    for (auto i = 0; i < m_size; i++)
        m_container[i] = orig.m_container[i];
}

template <typename TValue, int t_capacity>
TStorage<TValue, t_capacity>::~TStorage() {
    delete[] m_container;
}

template <typename TValue, int t_capacity>
void TStorage<TValue, t_capacity>::push_back(TValue &value) {
    if (m_size >= m_capacity)
        expand();
    
    m_container[m_size++] = value;
}
    
template <typename TValue, int t_capacity>
void TStorage<TValue, t_capacity>::remove(int index) {
    if (index >= m_size)
        throw EIndexOutOfBounds();
    
    for (auto i = index; i < m_size - 1; i++)
        m_container[i] = m_container[i + 1];
    m_size--;
}

template <typename TValue, int t_capacity>
void TStorage<TValue, t_capacity>::expand() {
    m_capacity == 0 ? m_capacity = 1 : m_capacity <<= 1;
    TValue *l_container = new TValue[m_capacity];
    
    for (auto i = 0; i < m_size; i++)
        l_container[i] = m_container[i];
    
    delete[] m_container;
    m_container = l_container;
}

template <typename TValue, int t_capacity>
void TStorage<TValue, t_capacity>::print_all() const {
    if (!m_size)
        cout << "Container is empty" << endl;
    
    for (auto i = 0; i < m_size; i++)
        cout << m_container[i] << " ";
    cout << endl;
}

template <typename TValue, int t_capacity>
void TStorage<TValue, t_capacity>::replace(int index, TValue &item) {
    if (index >= m_size)
        throw EIndexOutOfBounds();
    m_container[index] = item;
}

#endif	/* TSTORAGE_H */
