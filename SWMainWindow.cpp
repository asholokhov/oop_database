#include <QMessageBox>
#include <QStatusBar>
#include <QDebug>
#include <QTableWidgetItem>
#include <QFileDialog>
#include <QModelIndex>
#include <QInputDialog>

#include "SWMainWindow.h"
#include "TRecordAction.h"

SWMainWindow::SWMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_data_base(nullptr)
{
    ui->setupUi(this);
    m_status_bar = this->statusBar();
}

SWMainWindow::~SWMainWindow() {
    delete m_data_base;
}

void SWMainWindow::ask_for_save() {
    if (QMessageBox::information(this, tr("Info"), tr("Data base has been modified. Do you want to save it?"),
                                 QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes) {
        on_actionSave_triggered();
    }
}

void SWMainWindow::on_actionCredits_triggered()
{
    QMessageBox::about(this, tr("About"), tr("<h2>DB GUI 1.0.0</h2>"
                                             "<small>by Artem Sholokhov, 2013. SPb SUT, SP-01</small>"
                                             "<p>Provides works with software packets.</p>"));
}

void SWMainWindow::on_actionExit_triggered()
{
    if (m_data_base != nullptr)
        if (m_data_base->is_modified())
            ask_for_save();
    close();
}

void SWMainWindow::on_actionNew_triggered()
{
    if (m_data_base != nullptr) {
        if (m_data_base->is_modified())
            ask_for_save();

        delete m_data_base;
        m_data_base = nullptr;
    }

    m_data_base = new TDataBase();
    m_data_base->output_db(*ui->db_view);
    m_status_bar->showMessage(tr("New DB is created."), 2000);
}

void SWMainWindow::on_actionAdd_triggered()
{
    if (m_data_base == nullptr)
        return;

    TRecordAction l_ra_form(this);
    if (l_ra_form.exec() == QDialog::Accepted) {
        TDataBase::SSoftwarePacket *sp = l_ra_form.get_form_data();
        if (sp == nullptr) {
            QMessageBox::warning(this, tr("Attention"), tr("You must fill all fields."), QMessageBox::Ok);
            return;
        }

        m_data_base->add(*sp);
        delete sp;

        m_data_base->output_db(*ui->db_view);
    }
}

void SWMainWindow::on_actionRemove_triggered()
{
    if (m_data_base == nullptr)
        return;

    QModelIndex model_index = ui->db_view->currentIndex();
    int index = model_index.row();

    if (index < 0)
        return;

    QString message = "Do you really want to delete " + QString::number(index + 1) + " record?";

    if (QMessageBox::information(this, tr("Remove item"), tr(message.toStdString().c_str()),
                             QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes) {
        m_data_base->remove(index);
        m_data_base->output_db(*ui->db_view);
    }
}

void SWMainWindow::on_actionEdit_triggered()
{
    if (m_data_base == nullptr)
        return;

    QModelIndex model_index = ui->db_view->currentIndex();
    int index = model_index.row();

    if (index < 0)
        return;

    TRecordAction l_ra_form;
    l_ra_form.load_form_data(index, m_data_base->get_item(index));

    if (l_ra_form.exec() == QDialog::Accepted) {
        TDataBase::SSoftwarePacket *sp = l_ra_form.get_form_data();
        if (sp == nullptr) {
            QMessageBox::warning(this, tr("Attention"), tr("You must fill all fields."), QMessageBox::Ok);
            return;
        }

        m_data_base->edit(index, *sp);
        delete sp;

        m_data_base->output_db(*ui->db_view);
    }
}

void SWMainWindow::on_actionSave_triggered()
{
    QString file_name = QFileDialog::getSaveFileName(this, tr("Save base as..."));

    if (file_name.isEmpty())
        return;

    m_data_base->save(file_name.toStdString());
    QMessageBox::information(this, tr("Info"), tr("Base was saved"), QMessageBox::Ok);
}


void SWMainWindow::on_actionOpen_database_triggered()
{
    on_actionClose_triggered();

    QString file_name = QFileDialog::getOpenFileName(this, tr("Open base"));

    if (file_name.isEmpty())
        return;

    m_data_base = new TDataBase();
    m_data_base->load(file_name.toStdString());

    QMessageBox::information(this, tr("Info"), tr("Base was loaded"), QMessageBox::Ok);
    m_data_base->output_db(*ui->db_view);
}

void SWMainWindow::on_actionClose_triggered()
{
    if (m_data_base != nullptr)
        if (m_data_base->is_modified()) {
            ask_for_save();

            ui->db_view->setRowCount(0);

            delete m_data_base;
            m_data_base = nullptr;
        }
}

void SWMainWindow::on_actionMerge_triggered()
{
    QString file_name = QFileDialog::getOpenFileName(this, tr("Open base"));
    m_data_base->merge(file_name.toStdString());
    m_data_base->output_db(*ui->db_view);
    QMessageBox::information(this, tr("Info"), tr("Merge has been successful"), QMessageBox::Ok);
}

void SWMainWindow::on_actionFind_triggered()
{
    if (m_data_base == nullptr)
        return;

    QModelIndex model_index = ui->db_view->currentIndex();
    int index = model_index.row();

    bool ok = false;
    QString pattern = QInputDialog::getText(this, tr("Find dialog"), tr("What do you want to find?"),
                                            QLineEdit::Normal, "", &ok);

    if (!pattern.isEmpty() && ok) {
        int find = m_data_base->find(index, pattern.toStdString());
        if (find != -1) {
            ui->db_view->selectRow(find);
            m_status_bar->showMessage(tr("Finded"), 2000);
        } else {
            QMessageBox::warning(this, tr("Find"), tr("Not found!"), QMessageBox::Ok);
        }
    }
}
