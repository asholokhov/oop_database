/* 
 * File:   DataBase.cpp
 * Author: Artem
 * 
 * Created on 4 ������� 2013 �., 21:17
 */

#include "TDataBase.h"
#include <fstream>

#include <QTableWidgetItem>

TDataBase::~TDataBase() {
}

void TDataBase::add(SSoftwarePacket &item) {
    m_storage.push_back(item);
    m_has_modified = true;
}

void TDataBase::remove(int index) {
    try {
        m_storage.remove(index);
        m_has_modified = true;
    } catch (TStorage<SSoftwarePacket>::EIndexOutOfBounds e) {
        cerr << "Exeption: EIndexOutOfBounds in TDataBase::remove(" << index 
                << ")" << endl ;
    }
}

void TDataBase::edit(int index, SSoftwarePacket &new_item) {
    try {
        m_storage.replace(index, new_item);
        m_has_modified = true;
    } catch (TStorage<SSoftwarePacket>::EIndexOutOfBounds e) {
        cerr << "Exeption: EIndexOutOfBounds in TDataBase::replace(" << index 
                << ")" << endl ;
    }
}

int TDataBase::find(int start_index, string pattern) const {
    int find_index = start_index + 1;

    for (TStorage<SSoftwarePacket>::iterator it = m_storage.begin() + start_index + 1;
         it != m_storage.end(); ++it) {
        if ((*it).name.find(pattern) != string::npos ||
            (*it).developer.find(pattern) != string::npos ||
            (*it).vendor.find(pattern) != string::npos ||
            (*it).version.find(pattern) != string::npos ||
            (*it).packet_size.find(pattern) != string::npos ||
            (*it).date.find(pattern) != string::npos) {

            return find_index;

        }
        find_index++;
    }

    return -1;
}
    
void TDataBase::merge(string file_name) {
    load(file_name);
}
    
void TDataBase::save(string file_name) {
    ofstream out(file_name);
    
    for (auto i = 0; i < m_storage.size(); i++) {
        out << m_storage[i];
        if (i != m_storage.size() - 1)
           out << endl;
    }

    m_has_modified = false;
}

void TDataBase::load(string file_name) {
    ifstream in(file_name);
    
    SSoftwarePacket sp;
    while (!in.eof()) { 
        in >> sp;
        add(sp);
    }

    m_has_modified = true;
}

TDataBase::SSoftwarePacket& TDataBase::get_item(int index) const {
    return m_storage[index];
}

void TDataBase::output_db(QTableWidget &out) const {
    out.setRowCount(0);

    if (m_storage.size() == 0)
        return;

    for (TStorage<SSoftwarePacket>::iterator it = m_storage.begin(); it != m_storage.end(); ++it) {
        int row = out.rowCount();
        out.insertRow(row);

        QTableWidgetItem *item_name = new QTableWidgetItem(QString::fromStdString((*it).name));
        QTableWidgetItem *item_devl = new QTableWidgetItem(QString::fromStdString((*it).developer));
        QTableWidgetItem *item_vend = new QTableWidgetItem(QString::fromStdString((*it).vendor));
        QTableWidgetItem *item_vers = new QTableWidgetItem(QString::fromStdString((*it).version));
        QTableWidgetItem *item_psze = new QTableWidgetItem(QString::fromStdString((*it).packet_size));
        QTableWidgetItem *item_date = new QTableWidgetItem(QString::fromStdString((*it).date));

        out.setItem(row, 0, item_name);
        out.setItem(row, 1, item_devl);
        out.setItem(row, 2, item_vend);
        out.setItem(row, 3, item_vers);
        out.setItem(row, 4, item_psze);
        out.setItem(row, 5, item_date);
    }
}
